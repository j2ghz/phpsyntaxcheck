﻿Module Module1

    Sub Main()
        Dim celk As Integer = 0
        Dim usp As Integer = 0
        For Each file In My.Computer.FileSystem.GetFiles(My.Application.CommandLineArgs(0), FileIO.SearchOption.SearchAllSubDirectories, "*.php")
            celk += 1
            If php(file) Then usp += 1
        Next
        Console.WriteLine("Successful syntax checks: " & FormatNumber(((usp / celk) * 100), 2) & " covered")
    End Sub

    Function php(file As String) As Boolean
        Dim proc As New Process()
        proc.StartInfo = New ProcessStartInfo("php.exe", "-l " & file)
        With proc.StartInfo
            .UseShellExecute = False
            .RedirectStandardOutput = True
            .CreateNoWindow = True
        End With
        proc.Start()
        While Not proc.StandardOutput.EndOfStream
            Dim line As String = proc.StandardOutput.ReadLine
            Console.WriteLine("--" & line)
            If line.Contains("No syntax errors detected") Then Return True
        End While
        Return False
    End Function

End Module
